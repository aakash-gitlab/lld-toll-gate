package org.tollManagement.observer;

import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TollBooth;

public class GenericScoreUpdater implements ScoreUpdater {

    @Override
    public void update(BillingDetails billingDetails) {
        TollBooth tollBooth = billingDetails.getTollBooth();
        tollBooth.incrementBoothScore(1);
    }
}
