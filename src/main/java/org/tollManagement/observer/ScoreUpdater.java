package org.tollManagement.observer;

import org.tollManagement.beans.BillingDetails;

public interface ScoreUpdater {

    void update(BillingDetails billingDetails);
}
