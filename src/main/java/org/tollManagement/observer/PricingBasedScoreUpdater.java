package org.tollManagement.observer;

import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TollBooth;

public class PricingBasedScoreUpdater implements ScoreUpdater {

    @Override
    public void update(BillingDetails billingDetails) {
        TollBooth tollBooth = billingDetails.getTollBooth();
        if (billingDetails.getPrice() >= 100) {
            tollBooth.incrementBoothScore(20);
        }
        else if (billingDetails.getPrice() >= 50 && billingDetails.getPrice() < 100) {
            tollBooth.incrementBoothScore(10);
        } else if (billingDetails.getPrice() >= 1 && billingDetails.getPrice() < 50) {
            tollBooth.incrementBoothScore(5);
        }
    }
}
