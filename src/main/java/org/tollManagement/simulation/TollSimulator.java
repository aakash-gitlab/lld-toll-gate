package org.tollManagement.simulation;

import org.tollManagement.beans.Toll;
import org.tollManagement.beans.TollBooth;
import org.tollManagement.services.TollManagementService;

import java.util.ArrayList;
import java.util.List;

public class TollSimulator {

    public static void setUpTolls() {
        List<TollBooth> tollBooths1 = createTollBooths(100, 25);
        Toll toll1 = new Toll(1, tollBooths1);

        List<TollBooth> tollBooths2 = createTollBooths(200, 20);
        Toll toll2 = new Toll(2, tollBooths2);

        List<TollBooth> tollBooths3 = createTollBooths(300, 15);
        Toll toll3 = new Toll(3, tollBooths3);


        getTollManagementService().addToll(toll1);
        getTollManagementService().addToll(toll2);
        getTollManagementService().addToll(toll3);
    }


    private static List<TollBooth> createTollBooths(int startId, int totalCount) {
        List<TollBooth> tollBooths = new ArrayList<>();
        for (int i = startId; i < startId + totalCount; i++) {
            tollBooths.add(new TollBooth(i));
        }
        return tollBooths;
    }

    private static TollManagementService getTollManagementService() {
        return TollManagementService.getInstance();
    }
}
