package org.tollManagement.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Toll {

    private Integer tollId;

    private List<TollBooth> tollBooths;

    public Toll() {
    }

    public Toll(Integer tollId, List<TollBooth> tollBooths) {
        this.tollId = tollId;
        this.tollBooths = tollBooths;
    }


    public Integer getTollId() {
        return tollId;
    }

    public void setTollId(Integer tollId) {
        this.tollId = tollId;
    }

    public List<TollBooth> getTollBooths() {
        return tollBooths;
    }

    public void setTollBooths(List<TollBooth> tollBooths) {
        this.tollBooths = tollBooths;
    }

    public void addTollBooth(TollBooth tollBooth) {
        if (this.tollBooths == null) {
            this.tollBooths = new ArrayList<>();
        }
        this.tollBooths.add(tollBooth);
    }

    public TollBooth getTollBoothById(Integer tollBoothId) {
        if (tollBoothId == null || tollBooths.isEmpty()) {
            return null;
        }
        Map<Integer, TollBooth> map = tollBooths.stream().collect(Collectors.toMap(TollBooth::getId, Function.identity()));
        return map.get(tollBoothId);
    }
}
