package org.tollManagement.beans;


public class BillingDetails {

    private String billId;

    private TicketType ticketType;

    private TollBooth tollBooth;

    private long billIssueDate;

    private boolean isBillExpired;

    private long price;

    public BillingDetails(String billId, TicketType ticketType, TollBooth tollBooth) {
        this.billId = billId;
        this.ticketType = ticketType;
        this.tollBooth = tollBooth;
        this.billIssueDate = System.currentTimeMillis();
        this.isBillExpired = false;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public TollBooth getTollBooth() {
        return tollBooth;
    }

    public void setTollBooth(TollBooth tollBooth) {
        this.tollBooth = tollBooth;
    }

    public long getBillIssueDate() {
        return billIssueDate;
    }

    public void setBillIssueDate(long billIssueDate) {
        this.billIssueDate = billIssueDate;
    }

    public boolean isBillExpired() {
        return isBillExpired;
    }

    public void setBillExpired(boolean billExpired) {
        isBillExpired = billExpired;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "BillingDetails{" +
                "billId='" + billId + '\'' +
                ", ticketType=" + ticketType +
                ", tollBooth=" + tollBooth +
                ", billIssueDate=" + billIssueDate +
                ", isBillExpired=" + isBillExpired +
                ", price=" + price +
                '}';
    }
}
