package org.tollManagement.beans;

public enum TicketType {

    SINGLE_USE,
    ROUND_TRIP,
    WEEKLY_PASS;


}
