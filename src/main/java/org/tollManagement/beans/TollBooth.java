package org.tollManagement.beans;

public class TollBooth implements Comparable<TollBooth> {

    private Integer id;

    private Integer totalVehicleHandled;

    private Integer tollBoothScore;

    public TollBooth(Integer id) {
        this.id = id;
        totalVehicleHandled = 0;
        tollBoothScore = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalVehicleHandled() {
        return totalVehicleHandled;
    }

    public void setTotalVehicleHandled(int totalVehicleHandled) {
        this.totalVehicleHandled = totalVehicleHandled;
    }

    public void incrementVehicleHandled() {
        this.totalVehicleHandled += 1;
    }

    public int getTollBoothScore() {
        return tollBoothScore;
    }

    public void setTollBoothScore(int tollBoothScore) {
        this.tollBoothScore = tollBoothScore;
    }

    public void incrementBoothScore(int score) {
        this.tollBoothScore += score;
    }

    @Override
    public int compareTo(TollBooth o) {
        return o.tollBoothScore - this.tollBoothScore;
    }

    @Override
    public String toString() {
        return "TollBooth{" +
                "id=" + id +
                ", totalVehicleHandled=" + totalVehicleHandled +
                ", tollBoothScore=" + tollBoothScore +
                '}';
    }
}
