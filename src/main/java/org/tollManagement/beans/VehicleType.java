package org.tollManagement.beans;

public enum VehicleType {

    TWO_WHEELER,
    FOUR_WHEELER;
}
