package org.tollManagement.services;

import org.tollManagement.beans.TollBooth;
import org.tollManagement.exception.InvalidBoothException;

import java.util.List;

public interface BoothManagementService {

    TollBooth getTollBooth(Integer tollBoothId) throws InvalidBoothException;

    void addBoothToToll(TollBooth tollBooth) throws InvalidBoothException;

    void removeBoothFromToll(Integer tollBoothId) throws InvalidBoothException;

    List<TollBooth> getLeaderBoard();


}
