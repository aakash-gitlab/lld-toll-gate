package org.tollManagement.services;

import org.tollManagement.pricingStrategy.PriceGenerationFactory;
import org.tollManagement.pricingStrategy.PricingStrategy;
import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TicketType;
import org.tollManagement.beans.TollBooth;
import org.tollManagement.beans.Vehicle;
import org.tollManagement.billingStrategey.BillGenerationStrategy;
import org.tollManagement.billingStrategey.BillingGenerationFactory;
import org.tollManagement.observer.ScoreUpdater;

import java.util.Map;

import static org.tollManagement.utils.Util.generateRandomString;

public class BillingServiceImpl implements BillingService {

    private final ScoreUpdater tollBoothScoreUpdater;

    private BillingGenerationFactory billingGenerationFactory;

    private PriceGenerationFactory priceGenerationFactory;

    public BillingServiceImpl(ScoreUpdater tollBoothScoreUpdater) {
        this.tollBoothScoreUpdater = tollBoothScoreUpdater;
    }


    @Override
    public boolean processBilling(TollBooth tollBooth, Vehicle vehicle, TicketType ticketType) throws Exception {
        BillingDetails billingDetails = vehicle.getBillingDetails();
        boolean newBillGenerated = false;
        if (billingDetails == null || billingDetails.isBillExpired()) {
            billingDetails = doGenerateBill(tollBooth, vehicle, ticketType);
            newBillGenerated = true;
        } else {
            boolean newBillRequired = checkIfNewBillIsRequired(billingDetails);
            if (newBillRequired) {
                billingDetails = doGenerateBill(tollBooth, vehicle, ticketType);
                newBillGenerated = true;
            }
        }
        tollBooth.incrementVehicleHandled();
        vehicle.setBillingDetails(billingDetails);
        //update score of toll
        tollBoothScoreUpdater.update(billingDetails);
        return newBillGenerated;
    }

    @Override
    public Map<TicketType, Long> getPrice(Vehicle vehicle) {
        return getPricingStrategy(vehicle).getPrice();
    }


    //*****************************************************PRIVATE METHODS*********************************************

    private BillingDetails doGenerateBill(TollBooth tollBooth, Vehicle vehicle, TicketType ticketType) throws Exception {
        String billId = generateRandomString();
        BillingDetails billingDetails = new BillingDetails(billId, ticketType, tollBooth);
        Long price = calculatePrice(vehicle, billingDetails);
        billingDetails.setPrice(price);
        return billingDetails;
    }

    private Long calculatePrice(Vehicle vehicle, BillingDetails billingDetails) throws Exception {
        PricingStrategy pricingStrategy = getPricingStrategy(vehicle);
        return pricingStrategy.generatePrice(billingDetails);
    }

    private boolean checkIfNewBillIsRequired(BillingDetails billingDetails) {
        BillGenerationStrategy billingStrategy = getBillingStrategy(billingDetails);
        return billingStrategy.isBillingRequired(billingDetails);
    }

    private BillGenerationStrategy getBillingStrategy(BillingDetails billingDetails) {
        if (billingGenerationFactory == null) {
            billingGenerationFactory = new BillingGenerationFactory();
        }
        return billingGenerationFactory.getBillGenerationStrategy(billingDetails);
    }

    private PricingStrategy getPricingStrategy(Vehicle vehicle) {
        if (priceGenerationFactory == null) {
            priceGenerationFactory = new PriceGenerationFactory();
        }
        return priceGenerationFactory.getPricingStrategy(vehicle);
    }

}
