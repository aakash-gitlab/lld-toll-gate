package org.tollManagement.services;

import org.tollManagement.beans.Toll;
import org.tollManagement.exception.InvalidTollException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TollManagementService {

    private static TollManagementService TOLL_MANAGEMENT_SERVICE_INSTANCE = null;

    //in memory storage for all the tolls
    private Map<Integer, Toll> tollMap;

    public static synchronized TollManagementService getInstance()
    {
        if (TOLL_MANAGEMENT_SERVICE_INSTANCE == null) {
            TOLL_MANAGEMENT_SERVICE_INSTANCE = new TollManagementService();
        }
        return TOLL_MANAGEMENT_SERVICE_INSTANCE;
    }

    private TollManagementService() {
        tollMap = new HashMap<>();
        //singleton
    }


    public Toll getTollById(int id) throws InvalidTollException {
        validateIfTollExists(id);
        return tollMap.get(id);
    }

    public Collection<Toll> getAllTolls() {
        return tollMap.values();
    }

    public void addToll(Toll toll) {
        tollMap.put(toll.getTollId(), toll);
    }


    public void removeToll(Integer tollId) throws InvalidTollException {
        validateIfTollExists(tollId);
        tollMap.remove(tollId);
    }

    private void validateIfTollExists(Integer tollId) throws InvalidTollException {
        Toll toll = tollMap.get(tollId);
        if (toll == null) {
            throw new InvalidTollException("Toll Id not found in System");
        }
    }


}
