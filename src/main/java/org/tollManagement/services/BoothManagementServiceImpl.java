package org.tollManagement.services;

import org.apache.commons.collections4.CollectionUtils;
import org.tollManagement.beans.Toll;
import org.tollManagement.beans.TollBooth;
import org.tollManagement.exception.InvalidBoothException;

import java.util.*;
import java.util.stream.Collectors;

public class BoothManagementServiceImpl implements BoothManagementService {

    private final Toll toll;

    public BoothManagementServiceImpl(Toll toll) {
        this.toll = toll;
    }


    @Override
    public TollBooth getTollBooth(Integer tollBoothId) throws InvalidBoothException {
        validateIfBoothExists(tollBoothId);
        return toll.getTollBoothById(tollBoothId);
    }

    @Override
    public void addBoothToToll(TollBooth tollBooth) throws InvalidBoothException {
        if (tollBooth == null) {
            return;
        }
        validateIfBoothExistsInDifferentToll(tollBooth);
        toll.addTollBooth(tollBooth);
    }

    @Override
    public void removeBoothFromToll(Integer tollBoothId) throws InvalidBoothException {
        validateIfBoothExists(tollBoothId);
        //remove from toll

    }

    public List<TollBooth> getLeaderBoard() {
        List<TollBooth> tollBooths = toll.getTollBooths();
        Collections.sort(tollBooths);
        return tollBooths;
    }


    //*****************************************************PRIVATE METHODS*********************************************

    private void validateIfBoothExistsInDifferentToll(TollBooth tollBooth) throws InvalidBoothException {
        Collection<Toll> allTolls = TollManagementService.getInstance().getAllTolls();
        if (allTolls.isEmpty()) {
            throw new InvalidBoothException("No Toll Exists");
        }
        for (Toll toll : allTolls) {
            if (!Objects.equals(toll.getTollId(), this.toll.getTollId())) {
                List<TollBooth> tollBooths = toll.getTollBooths();
                if (CollectionUtils.isNotEmpty(tollBooths)) {
                    List<Integer> tollBoothIds = tollBooths.stream().map(TollBooth::getId).collect(Collectors.toList());
                    if (tollBoothIds.contains(tollBooth.getId())) {
                        throw new InvalidBoothException("Toll Booth already register in toll: " + toll.getTollId());
                    }
                }
            }
        }
    }

    private void validateIfBoothExists(Integer tollId) throws InvalidBoothException {
        TollBooth tollBooth = toll.getTollBoothById(tollId);
        if (tollBooth == null) {
            throw new InvalidBoothException("Toll Id not found in System");
        }
    }



}
