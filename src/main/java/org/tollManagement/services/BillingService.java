package org.tollManagement.services;

import org.tollManagement.beans.TicketType;
import org.tollManagement.beans.TollBooth;
import org.tollManagement.beans.Vehicle;

import java.util.Map;

public interface BillingService {

    public boolean processBilling(TollBooth tollBooth, Vehicle vehicle, TicketType ticketType) throws Exception;

    public Map<TicketType, Long> getPrice(Vehicle vehicle);
}
