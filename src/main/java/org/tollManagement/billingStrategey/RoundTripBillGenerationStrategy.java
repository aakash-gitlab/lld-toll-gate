package org.tollManagement.billingStrategey;

import org.tollManagement.beans.BillingDetails;

public class RoundTripBillGenerationStrategy implements BillGenerationStrategy {


    @Override
    public boolean isBillingRequired(BillingDetails billingDetails) {
        billingDetails.setBillExpired(true);
        return false;
    }
}
