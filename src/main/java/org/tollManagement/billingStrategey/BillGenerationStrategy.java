package org.tollManagement.billingStrategey;

import org.tollManagement.beans.BillingDetails;

public interface BillGenerationStrategy {

    public boolean isBillingRequired(BillingDetails billingDetails);


}
