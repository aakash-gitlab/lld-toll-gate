package org.tollManagement.billingStrategey;

import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TicketType;

public class BillingGenerationFactory {


    public BillGenerationStrategy getBillGenerationStrategy(BillingDetails billingDetails) {
        if (TicketType.SINGLE_USE.equals(billingDetails.getTicketType())) {
            return new OneTimeBillGenerationStrategy();
        } else if (TicketType.ROUND_TRIP.equals(billingDetails.getTicketType())) {
            return new RoundTripBillGenerationStrategy();
        } else if (TicketType.WEEKLY_PASS.equals(billingDetails.getTicketType())) {
            return new WeeklyBillGenerationStrategy();
        }
        return null;

    }
}
