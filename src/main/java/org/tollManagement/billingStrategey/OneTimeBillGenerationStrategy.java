package org.tollManagement.billingStrategey;

import org.tollManagement.beans.BillingDetails;

public class OneTimeBillGenerationStrategy implements BillGenerationStrategy{

    @Override
    public boolean isBillingRequired(BillingDetails billingDetails) {
        billingDetails.setBillExpired(true);
        return true;
    }
}
