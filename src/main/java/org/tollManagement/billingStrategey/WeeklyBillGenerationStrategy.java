package org.tollManagement.billingStrategey;

import org.tollManagement.beans.BillingDetails;

import java.util.concurrent.TimeUnit;

public class WeeklyBillGenerationStrategy implements BillGenerationStrategy {

    @Override
    public boolean isBillingRequired(BillingDetails billingDetails) {
        long billIssueDate = billingDetails.getBillIssueDate();

        long totalTime = System.currentTimeMillis() - billIssueDate;
        boolean isDateValid = TimeUnit.DAYS.toMillis(7) > totalTime;
        if (isDateValid) {
            return false;
        } else {
            billingDetails.setBillExpired(true);
            return true;
        }

    }
}
