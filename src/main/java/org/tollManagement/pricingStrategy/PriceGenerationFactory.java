package org.tollManagement.pricingStrategy;

import org.tollManagement.beans.Vehicle;
import org.tollManagement.beans.VehicleType;

public class PriceGenerationFactory {

    public PricingStrategy getPricingStrategy(Vehicle vehicle) {
        VehicleType vehicleType = vehicle.getVehicleType();
        if (VehicleType.TWO_WHEELER.equals(vehicleType)) {
            return new TwoWheelerPricing();
        } else if (VehicleType.FOUR_WHEELER.equals(vehicleType)) {
            return new FourWheelerPricingStrategy();
        }
        return null;
    }
}
