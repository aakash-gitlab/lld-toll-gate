package org.tollManagement.pricingStrategy;

import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TicketType;

import java.util.HashMap;
import java.util.Map;

public class TwoWheelerPricing implements PricingStrategy {

    @Override
    public Long generatePrice(BillingDetails billingDetails) throws Exception {
        if (TicketType.SINGLE_USE.equals(billingDetails.getTicketType())) {
            return 10L;
        }
        if (TicketType.ROUND_TRIP.equals(billingDetails.getTicketType())) {
            return 20L;
        }
        if (TicketType.WEEKLY_PASS.equals(billingDetails.getTicketType())) {
            return 100L;
        }
        throw new Exception("Invalid bill type");
    }

    @Override
    public Map<TicketType, Long> getPrice() {
        Map<TicketType, Long> priceMap = new HashMap<>();
        priceMap.put(TicketType.SINGLE_USE, 10L);
        priceMap.put(TicketType.ROUND_TRIP, 20L);
        priceMap.put(TicketType.WEEKLY_PASS, 100L);
        return priceMap;
    }
}
