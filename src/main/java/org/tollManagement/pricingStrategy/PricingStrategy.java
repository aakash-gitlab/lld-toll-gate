package org.tollManagement.pricingStrategy;

import org.tollManagement.beans.BillingDetails;
import org.tollManagement.beans.TicketType;

import java.util.Map;

public interface PricingStrategy {


    Long generatePrice(BillingDetails billingDetails) throws Exception;

    Map<TicketType, Long> getPrice();



}
