package org.tollManagement.exception;

public class InvalidTollException extends RuntimeException{

    public InvalidTollException() {
    }

    public InvalidTollException(String message) {
        super(message);
    }
}
