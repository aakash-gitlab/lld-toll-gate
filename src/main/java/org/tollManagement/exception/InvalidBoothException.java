package org.tollManagement.exception;

public class InvalidBoothException extends RuntimeException {

    public InvalidBoothException() {
    }

    public InvalidBoothException(String message) {
        super(message);
    }
}
