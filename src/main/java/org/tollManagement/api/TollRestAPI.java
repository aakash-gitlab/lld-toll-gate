package org.tollManagement.api;

import org.tollManagement.beans.Toll;
import org.tollManagement.services.TollManagementService;

import javax.ws.rs.*;

@Path("/toll")
public class TollRestAPI {

    TollManagementService tollMangamentService;

    public TollRestAPI(TollManagementService tollMangamentService) {
        this.tollMangamentService = tollMangamentService;
    }

    @GET
    @Path("/{tollId}")
    public Toll getToll(@QueryParam("tollId") int tollId) throws Exception {
        return tollMangamentService.getTollById(tollId);
    }

    @POST
    @Path("/")
    public void registerToll(Toll toll) {
         tollMangamentService.addToll(toll);
    }

    @DELETE
    @Path("/{tollId}")
    public void deleteToll(@QueryParam("tollId") int tollId) throws Exception {
        tollMangamentService.removeToll(tollId);
    }
}
