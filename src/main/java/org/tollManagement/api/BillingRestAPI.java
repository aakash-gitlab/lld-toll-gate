package org.tollManagement.api;

import org.tollManagement.beans.TicketType;
import org.tollManagement.beans.TollBooth;
import org.tollManagement.beans.Vehicle;
import org.tollManagement.services.BillingService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.Map;

@Path("/bill")
public class BillingRestAPI {

    BillingService billingService;

    public BillingRestAPI(BillingService billingService) {
        this.billingService = billingService;
    }

    @GET
    @Path("/get-prices")
    public Map<TicketType, Long> getPrices(Vehicle vehicle) {
        return billingService.getPrice(vehicle);
    }

    @POST
    @Path("/generate-bill")
    public boolean generateBill(TollBooth tollBooth, Vehicle vehicle, TicketType ticketType) throws Exception {
        return billingService.processBilling(tollBooth, vehicle, ticketType);
    }
}
