package org.tollManagement.api;

import org.tollManagement.beans.TollBooth;
import org.tollManagement.services.BoothManagementServiceImpl;

import javax.ws.rs.*;

@Path("/toll")
public class TollBoothRestAPI {

    BoothManagementServiceImpl boothManagementService;

    public TollBoothRestAPI(BoothManagementServiceImpl boothManagementService) {
        this.boothManagementService = boothManagementService;
    }

    @GET
    @Path("/{boothId}")
    public TollBooth getToll(@QueryParam("boothId") int boothId) throws Exception {
        return boothManagementService.getTollBooth(boothId);
    }

    @POST
    @Path("/")
    public void registerToll(TollBooth tollBooth) {
        boothManagementService.addBoothToToll(tollBooth);
    }

    @DELETE
    @Path("/{tollId}")
    public void deleteToll(@QueryParam("tollBoothId") int tollBoothId) throws Exception {
        boothManagementService.removeBoothFromToll(tollBoothId);
    }
}
