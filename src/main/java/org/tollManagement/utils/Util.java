package org.tollManagement.utils;

import java.util.Random;
import java.util.UUID;

public class Util {

    public static String generateRandomString() {
        return UUID.randomUUID().toString();
    }

    public static int generateRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }
}
