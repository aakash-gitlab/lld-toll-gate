package org.tollManagement;

import org.tollManagement.beans.*;
import org.tollManagement.observer.GenericScoreUpdater;
import org.tollManagement.observer.ScoreUpdater;
import org.tollManagement.services.*;
import org.tollManagement.simulation.TollSimulator;
import org.tollManagement.utils.Util;

import java.util.List;
import java.util.Map;

public class Main {

    private static BillingService billingService;

    public static void main(String[] args) throws Exception{

        TollSimulator.setUpTolls();
        Toll toll1 = getTollManagementService().getTollById(1);

        //starting multiple trips of the vehicle with ROUND_TRIP ticket
        startRoundTripOfVehicle(toll1);

        System.out.println("\n\n");
        System.out.println("Toll Scoreboard for Toll " +  toll1.getTollId());
        System.out.println("Toll Booth Id\t\t\t Score");

        List<TollBooth> leaderBoard = new BoothManagementServiceImpl(toll1).getLeaderBoard();
        for (TollBooth tollBooth : leaderBoard) {
            System.out.println(tollBooth.getId() + "\t\t\t\t\t\t\t" + tollBooth.getTollBoothScore());
        }


        //running multiple vehicles with one time tickets to calculate the scoreboard
        Toll toll2 = getTollManagementService().getTollById(2);
        startOneWayTripsForVehicle(toll2);
        System.out.println("\n\n");
        System.out.println("Toll Scoreboard for Toll " +  toll2.getTollId());
        System.out.println("Toll Booth Id\t\t\t Score");

        List<TollBooth> leaderBoard2 = new BoothManagementServiceImpl(toll2).getLeaderBoard();

        for (TollBooth tollBooth : leaderBoard2) {
            System.out.println(tollBooth.getId() + "\t\t\t\t\t\t\t" + tollBooth.getTollBoothScore());
        }
    }

    private static void startRoundTripOfVehicle(Toll toll) throws Exception {
        Vehicle vehicle = getVehicle("KA02AN8589", VehicleType.FOUR_WHEELER);
        List<TollBooth> tollBooths = toll.getTollBooths();

        //Displaying the price based on ticket type
        Map<TicketType, Long> price = getBillingService().getPrice(vehicle);
        System.out.println("Price Details for " + vehicle.getVehicleType() + " below: ");
        System.out.println(price + "\n\n");


        TollBooth tollBooth = tollBooths.get(0);
        boolean billGenerated = billingService.processBilling(tollBooth, vehicle, TicketType.ROUND_TRIP);
        System.out.println("Bill Generated for 1st trip of Round trip " + billGenerated);

        boolean billGeneratedForSecondTrip = billingService.processBilling(tollBooth, vehicle, TicketType.ROUND_TRIP);
        System.out.println("Bill Generated for 2nd trip of Round trip " + billGeneratedForSecondTrip);

        boolean billGeneratedForThirdTrip = billingService.processBilling(tollBooth, vehicle, TicketType.ROUND_TRIP);
        System.out.println("Bill Generated for 3rd trip of Round trip " + billGeneratedForThirdTrip);

        //Passing all the three trips via same toll booth.
        System.out.println("Toll Booth " + tollBooth.getId() + " Score: " + tollBooth.getTollBoothScore() + ", Total Vehicles Handled: " + tollBooth.getTotalVehicleHandled());
    }


    private static void startOneWayTripsForVehicle(Toll toll) throws Exception {
        for (int i = 5000; i < 5100; i++) {
            Vehicle vehicle = getVehicle("KA05ET" + i, VehicleType.TWO_WHEELER);
            List<TollBooth> tollBooths = toll.getTollBooths();
            TollBooth tollBooth = tollBooths.get(Util.generateRandomNumber(0,19));

            billingService.processBilling(tollBooth, vehicle, TicketType.ROUND_TRIP);
        }
    }


    private static BillingService getBillingService() {
        if (billingService == null) {
            ScoreUpdater scoreUpdater = new GenericScoreUpdater();
            billingService = new BillingServiceImpl(scoreUpdater);
        }
        return billingService;
    }

    private static Vehicle getVehicle(String registrationNumber, VehicleType vehicleType) {
        return new Vehicle(registrationNumber, vehicleType);
    }

    private static TollManagementService getTollManagementService() {
        return TollManagementService.getInstance();
    }

}