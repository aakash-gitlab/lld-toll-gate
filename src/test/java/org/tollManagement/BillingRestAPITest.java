package org.tollManagement;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.tollManagement.api.BillingRestAPI;
import org.tollManagement.beans.TicketType;
import org.tollManagement.beans.Vehicle;
import org.tollManagement.beans.VehicleType;
import org.tollManagement.observer.GenericScoreUpdater;
import org.tollManagement.services.BillingService;
import org.tollManagement.services.BillingServiceImpl;

import java.util.Map;

public class BillingRestAPITest {

    BillingRestAPI billingRestAPI;


    @Test
    public void testPriceListGeneration() {
        Map<TicketType, Long> priceMap = getBillingRestAPI().getPrices(getVehicle("TEST_REGN_NO", VehicleType.TWO_WHEELER));
        Assertions.assertNotNull(priceMap);
        Assertions.assertTrue(priceMap.containsKey(TicketType.SINGLE_USE));
    }




    private BillingRestAPI getBillingRestAPI() {
        if (billingRestAPI == null) {
            BillingService billingService = new BillingServiceImpl(new GenericScoreUpdater());
            billingRestAPI = new BillingRestAPI(billingService);
        }
        return billingRestAPI;

    }


    private static Vehicle getVehicle(String registrationNumber, VehicleType vehicleType) {
        return new Vehicle(registrationNumber, vehicleType);
    }
}
